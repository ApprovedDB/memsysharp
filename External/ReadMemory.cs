﻿using System;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace MemsySharp.External
{
    public static class ReadMemory
    {
        public static T ReadValue<T>(this IntPtr ptr, int length = 0, int timeout = 4)
        {
            int rCount = 0;
            while (true)
            {
                Type valT = typeof(T);
                byte[] buffer = length > 0 ? new byte[length] : new byte[Marshal.SizeOf(valT)];
                IntPtr retBytes;
                object result;

                if (!Memory.ReadProcessMemory(ActiveMemoryReference.CurrentProcessHandle, ptr, buffer, buffer.Length, out retBytes))
                {
                    Memory.OpenProcess(ActiveMemoryReference.CurrentProcessName);
                    if (rCount >= timeout)
                        return default(T);
                    
                    rCount++;
                    continue;
                }

                if (valT == typeof(byte))
                    result = buffer[0];
                else if (valT == typeof(bool))
                    result = buffer[0] > 0;
                else if (valT == typeof(char))
                    result = BitConverter.ToChar(buffer, 0);
                else if (valT == typeof(double))
                    result = BitConverter.ToDouble(buffer, 0);
                else if (valT == typeof(float))
                    result = BitConverter.ToSingle(buffer, 0);
                else if (valT == typeof(int))
                    result = BitConverter.ToInt32(buffer, 0);
                else if (valT == typeof(long))
                    result = BitConverter.ToInt64(buffer, 0);
                else if (valT == typeof(object))
                    result = buffer;
                else if (valT == typeof(short))
                    result = BitConverter.ToInt16(buffer, 0);
                else if (valT == typeof(string))
                {
                    string s = Encoding.UTF8.GetString(buffer, 0, buffer.Length);
                    string[] stringVal = s.Split(new char[] {'\0'}, StringSplitOptions.RemoveEmptyEntries);
                    result = stringVal.Length > 0 ? stringVal[0] : string.Empty;
                }
                else if (valT == typeof(uint))
                    result = BitConverter.ToUInt32(buffer, 0);
                else if (valT == typeof(ulong))
                    result = BitConverter.ToUInt64(buffer, 0);
                else if (valT == typeof(ushort))
                    result = BitConverter.ToUInt16(buffer, 0);
                else
                {
                    IntPtr newVal = Marshal.AllocHGlobal(buffer.Length);
                    Marshal.Copy(buffer, 0, newVal, buffer.Length);
                    result = newVal;
                    Marshal.FreeHGlobal(newVal);
                }

                return (T) result;
            }
        }
        
        public static IntPtr GetPointer(int baseAddress, int offset, int length)
        {
            byte[] buffer = new byte[length];
            IntPtr retBytes;
            IntPtr ptr = new IntPtr(baseAddress + offset);
            if (!Memory.ReadProcessMemory(ActiveMemoryReference.CurrentProcessHandle, ptr, buffer, buffer.Length, out retBytes))
                return IntPtr.Zero;

            buffer.Reverse();
            return new IntPtr(BitConverter.ToInt32(buffer, 0));
        }
        
        public static IntPtr GetPointer(int address, int length)
        {
            byte[] buffer = new byte[length];
            IntPtr retBytes;
            IntPtr ptr = new IntPtr(address);
            if (!Memory.ReadProcessMemory(ActiveMemoryReference.CurrentProcessHandle, ptr, buffer, buffer.Length, out retBytes))
                return IntPtr.Zero;

            buffer.Reverse();
            return new IntPtr(BitConverter.ToInt32(buffer, 0));
        }
        
        public static IntPtr GetPointer(IntPtr address, int length, int timeout = 4)
        {
            int rCount = 0;
            while (true)
            {
                byte[] buffer = new byte[length];
                IntPtr retBytes;
                if (!Memory.ReadProcessMemory(ActiveMemoryReference.CurrentProcessHandle, address, buffer,
                    buffer.Length, out retBytes))
                {
                    Console.WriteLine($"Error: {Marshal.GetLastWin32Error()}");
                    if (rCount >= timeout)
                        return IntPtr.Zero;
                    
                    Memory.OpenProcess(ActiveMemoryReference.CurrentProcessName);
                    
                    rCount++;
                    continue;
                }

                buffer.Reverse();
                return new IntPtr(BitConverter.ToInt32(buffer, 0));
            }
        }
    }
}