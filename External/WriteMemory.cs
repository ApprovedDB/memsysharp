﻿using System;
using System.Runtime.InteropServices;

namespace MemsySharp.External
{
    public static class WriteMemory
    {
        public static bool WriteValue<T>(this IntPtr ptr, T value, int length = 0)
        {
            byte[] buffer = length > 0 ? new byte[length] : new byte[Marshal.SizeOf(typeof(T))];
            IntPtr retBytes;
            return Memory.WriteProcessMemory(ActiveMemoryReference.CurrentProcessHandle, ptr, buffer, buffer.Length, out retBytes);
        }
    }
}