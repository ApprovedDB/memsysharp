﻿using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace MemsySharp.External
{
    public static class Memory
    {
        #region DllImports
        [DllImport("kernel32.dll")]
        public static extern IntPtr OpenProcess(int dwDesiredAccess, bool bInheritHandle, int dwProcessId);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteProcessMemory(IntPtr hProcess, IntPtr lpBaseAddress, byte[] lpBuffer, int dwSize, out IntPtr lpNumberOfBytesRead);

        [DllImport("user32.dll", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);
        #endregion
        
        public static bool OpenProcess(string processName)
        {
            if (processName == "")
                return false;

            ActiveMemoryReference.CurrentProcessName = processName;
            Process[] process = Process.GetProcessesByName(processName);
            
            if (process.Length <= 0)
                return false;
            
            ActiveMemoryReference.CurrentProcessHandle = process[0].Handle;
            ActiveMemoryReference.CurrentProcessId = process[0].Id;
            ActiveMemoryReference.EntryPointAddress = process[0].MainModule.BaseAddress.ToInt32();
            return true;
        }
    }
    
    public struct ActiveMemoryReference
    {
        public static string CurrentProcessName;
        public static IntPtr CurrentProcessHandle;
        public static int EntryPointAddress;
        public static int CurrentProcessId;
    }
}